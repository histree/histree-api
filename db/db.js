const Sequelize = require('sequelize');
var db = {};
var connectionString = "postgres://" + process.env.PGUSER + ":" + process.env.PGPASSWORD + "@" + process.env.PGHOST + ":" + process.env.PGPORT + "/" + process.env.PGDATABASE;
const sequelize = new Sequelize(connectionString, {
    dialect: 'postgres',
    pool: {
        max: 5,
        min: 0,
        acquire: 30000,
        idle: 10000
    },
    operatorsAliases: false,
    ssl: true,
    dialectOptions: {
        ssl: true
    }
});

db.Person = sequelize.define('person', {
    id: {
        type: Sequelize.BIGINT,
        primaryKey: true, 
        autoIncrement: true,
        allowNull: false
    },
    first_name: {
        type: Sequelize.STRING
    },
    last_name: {
        type: Sequelize.STRING
    },
    email: {
        type: Sequelize.STRING,
        unique: true
    },
    password_hash: {
        type:Sequelize.TEXT
    }
},
{
    underscored: true,
    freezeTableName: true
});

db.Topic = sequelize.define('topic', {
    id: {
        type: Sequelize.BIGINT,
        primaryKey: true,
        autoIncrement: true,
        allowNull: false,
    },
    name: {
        type: Sequelize.STRING
    },
    description: {
        type: Sequelize.TEXT
    },
    start: {
        type: Sequelize.DATE
    },
    image: {
        type: Sequelize.TEXT
    },
    created_at: {
        type: Sequelize.DATE
    },
    updated_at: {
        type: Sequelize.DATE
    }
},
{
    underscored: true,
    freezeTableName: true
});

module.exports = {
  db: db
};