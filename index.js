var dotenv = require('dotenv');
dotenv.config();
var express = require('express');
var bodyParser = require('body-parser');
var loginRouter = require('./routes/login/login');
var dashboardRouter = require('./routes/dashboard/dashboard');
var passport = require('passport');
var cookieParser = require('cookie-parser');
var session = require('express-session');
var cors = require('cors');
var app = express();

app.use(cors({
    origin: true,
    credentials: true
}));
app.use(bodyParser.urlencoded({extended : true}))
app.use(cookieParser());
app.use(bodyParser.json());
app.use(session({secret : 'keyboard cat'}));
app.use(passport.initialize());
app.use(passport.session());
app.use('/login', loginRouter);
app.use('/dashboard', dashboardRouter);

app.set('port', process.env.PORT || 3000);
app.listen(app.get('port'));
console.log('Server listening on port ' + app.get('port'));
