var express = require('express');
var router = express.Router();
var { db } = require('../../db/db');

router.get('/dashboard', 
    function(req, res) {
        res.send({user : req.user});
    }
);

router.post('/topics', function(req, res) {
    (async (req, res) => {
        var topics = await db.Topic.findAll();
        res.send({topics: topics});
    })(req, res);
});

module.exports = router;