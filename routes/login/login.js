var express = require('express');
var router = express.Router();
var { db } = require('../../db/db');
var bcrypt = require('bcrypt');
var passport = require('passport');
var localStrategy = require('passport-local');

passport.use(new localStrategy({usernameField : 'email'},
    function (email, password, done) {
        (async(email, password, done) => {
            var people = await db.Person.findAll({
                where: {
                    email : email
                }
            });
            if (people.length != 1) {
                return done(null, false);
            }
            var user = people[0];
            var match = await bcrypt.compare(password, user.password_hash);
            if (match) {
                return done(null, user);
            } else {
                return done(null, false);
            }
        })(email, password, done);
    }
));

passport.serializeUser(function(user, done) {
    done(null, user.id);
});

passport.deserializeUser((id, done) => {
    console.log('deserializing');
    (async(id, done) => {
       var person = await db.Person.findById(id);
       if (!person) {
            return done(null);
        } else {
            return done(null, person);
        }
    })(id, done);
});

loginValidate = (req, res, next) => {
    var body = req.body;
    if (!body.email || !body.password) {
        return res.status(400).send();
    } else {
        next();
    }
}

router.post('/login', loginValidate, passport.authenticate('local'), 
    function(req, res) {
        res.send(200, {
            user: req.user
        });
    }
);

createUserValidate = (req, res, next) => {
    var body = req.body;
    if (!body.email || !body.firstName || !body.lastName || !body.password) {
        return res.status(400).send();
    } else {
        next();
    }
};

router.post('/createUser', createUserValidate, function(req, res) {
    (async (req, res) => {
        try {
            var user = req.body;
            var existingUsers = await db.Person.findAll({
                where: {
                    email : user.email
                }
            });
            if (existingUsers.length > 0) {
                return res.status(409).send({message: 'An account with this email already exists'});
            } else {
                var hash = await bcrypt.hash(user.password, 10);
                var result = await db.Person.create({
                    email: user.email,
                    first_name: user.firstName,
                    last_name: user.lastName,
                    password_hash: hash
                });
                return res.status(200).send({message: 'Account created'});
            }
        } catch (exception) {
            res.status(500).send({message: 'Internal Server Error'});
            throw exception;
        }
    })(req, res);
});

router.post('/logout', function(req, res) {
    req.logout();
    res.status(200).send();
});

router.get('/authenticated', function(req, res) {
    res.send({authenticated: !!req.user });
});

module.exports = router;